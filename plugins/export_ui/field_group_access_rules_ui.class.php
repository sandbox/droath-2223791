<?php

/**
 * @file
 * The additional edit form elements for the multi-step wizard form.
 */

class field_group_access_rules_ui extends ctools_export_ui {

  /**
   * The form elements for the context section of the multi-step form.
   */
  public function edit_form_context(&$form, &$form_state) {
    ctools_include('context-admin');
    ctools_context_admin_includes();

    $module = 'export_ui::' . $this->plugin['name'];
    $name = $this->edit_cache_get_key($form_state['item'], $form_state['form type']);
    ctools_context_add_required_context_form($module, $form, $form_state, $form['required_contexts_table'], $form_state['item'], $name);
  }

  /**
   * The form elements for the access section of the multi-step form.
   */
  public function edit_form_access(&$form, &$form_state) {
    ctools_include('context-access-admin');

    $cache_key = $form_state['object']->edit_cache_get_key($form_state['item'], $form_state['form type']);
    $access_state = array(
      'access' => $form_state['item']->access,
      'module' => 'ctools_export_ui',
      'callback argument' => $form_state['object']->plugin['name'] . ':' . $cache_key,
      'contexts' => field_group_access_get_context_by_rule($form_state['item']),
      'no buttons' => TRUE,
    ) + $form_state;

    $form = ctools_access_admin_form($form, $access_state);
  }

  // Submission method for the access section of the multi-step form.
  public function edit_form_access_submit(&$form, &$form_state) {
    $form_state['item']->access['logic'] = $form_state['values']['logic'];
  }
}

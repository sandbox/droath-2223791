<?php

/**
 * @file
 * The export UI plugin implementations for the field group access module.
 */

$plugin = array(
  'label'  => t('Field Group Access Rules'),

  // Set the access permission.
  'access' => 'administer field group access rules',

  // Set the schema table name where the data is stored.
  'schema' => 'field_group_access_rules',

  // Define the menu information.
  'menu' => array(
    'menu prefix'      => 'admin/config/content',
    'menu item'        => 'field_group_access_rules',
    'menu title'       => t('Field group access rules'),
    'menu description' => t('Configure field group access rules.'),
  ),

  // Define the title's singular and plural definitions.
  'title singular'        => t('access rule'),
  'title singular proper' => t('Access Rule'),
  'title plural'          => t('access rules'),
  'title plural proper'   => t('Access Rules'),

  'use wizard' => TRUE,
  'form info' => array(
    'order' => array(
      'basic'   => t('Basic information'),
      'context' => t('Context'),
      'access'  => t('Access'),
    ),
  ),
  'handler' => 'field_group_access_rules_ui',
);
